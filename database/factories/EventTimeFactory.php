<?php

/** @var Factory $factory */

use App\Event;
use App\EventTime;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Carbon;

$factory->define(EventTime::class, function () {

    static $hour = 8;
    static $weekDay = 0;

    // hour should be between 08:00-21:00
    $hour = ($hour % 21 === 1 ? 8 : $hour);
    // week_day should be between 0-6
    $weekDay = ($weekDay === 7 ? 0 : $weekDay);

    return [
        'event_id' => function() {
            return factory(Event::class)->create()->id;
        },
        'week_day' => $weekDay++,
        'time' => Carbon::createFromTimeString($hour++ . ':00')->format('H:i'),
    ];
});
