<?php

/** @var Factory $factory */

use App\EventCustom;
use App\EventCustomTime;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Carbon;

$factory->define(EventCustomTime::class, function () {

    static $hour = 8;

    // hour should be between 08:00-21:00
    $hour = $hour % 21 === 1 ? 8 : $hour;

    return [
        'event_custom_id' => function() {
            return factory(EventCustom::class)->create()->id;
        },
        'time' => Carbon::createFromTimeString($hour++ . ':00')->format('H:i'),
    ];
});
