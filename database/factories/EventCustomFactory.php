<?php

/** @var Factory $factory */

use App\Event;
use App\EventCustom;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Carbon;

$factory->define(EventCustom::class, function (Faker $faker) {

    static $day = 1;

    return [
        'event_id' => function() {
            return factory(Event::class)->create()->id;
        },
        'date' => Carbon::now()->addMonth()->addDays($day++)->format('Y-m-d'),
    ];
});
