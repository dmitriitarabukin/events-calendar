<?php

/** @var Factory $factory */

use App\EventBid;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Carbon;

$factory->define(EventBid::class, function (Faker $faker) {

    static $hour = 8;

    // hour should be between 08:00-21:00
    $hour = $hour % 21 === 1 ? 8 : $hour;

    return [
        'event_id' => function() {
            return factory(\App\Event::class)->create()->id;
        },
        'date' => $faker->dateTimeBetween('now', '+14 days')->format('Y-m-d'),
        'time' => Carbon::createFromTimeString($hour++ . ':00')->format('H:i'),
        'name' => $faker->name(),
        'contact' => $faker->phoneNumber,
    ];
});
