<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventCustomTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_custom_times', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('event_custom_id');
            $table->time('time');
            $table->unique(['event_custom_id', 'time']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_custom_times');
    }
}
