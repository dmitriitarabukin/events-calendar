<?php

use App\Event;
use App\EventTime;
use Illuminate\Database\Seeder;

class EventTimesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Event::class, 2)
            ->create()
            ->each(function(Event $event) {
                $event
                    ->eventTimes()
                    ->saveMany(factory(EventTime::class, 5)->create());
        });
    }
}
