<?php

use App\Event;
use App\EventBid;
use App\EventCustom;
use App\EventCustomTime;
use App\EventTime;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(Event::class, 2)
            ->create()
            ->each(function(Event $event) {
                $event
                    ->eventTimes()
                    ->saveMany(factory(EventTime::class, 2)->make(['event_id' => $event->id]));
                $event
                    ->eventCustoms()
                    ->saveMany(factory(EventCustom::class, 5)->create(['event_id' => $event->id])
                        ->each(function(EventCustom $eventCustom) {
                            $eventCustom
                                ->eventCustomTimes()
                                ->saveMany(factory(EventCustomTime::class, 5)->make(['event_custom_id' => $eventCustom->id]));
                        }));
                // Store bidden event times
                $event
                    ->eventTimes()
                    ->inRandomOrder()
                    ->limit(2)
                    ->get()
                    ->each(function(EventTime $eventTime) use ($event) {
                        $event
                            ->eventBids()
                            ->save(factory(EventBid::class)->make([
                                'event_id' => $event->id,
                                'date' => Carbon::now()->next((int)$eventTime->week_day)->format('Y-m-d'),
                                'time' => $eventTime->time,
                            ]));
                    });
            });
    }
}
