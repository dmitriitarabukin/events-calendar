<?php

use App\EventCustom;
use App\EventCustomTime;
use Illuminate\Database\Seeder;

class EventCustomTimesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(EventCustom::class, 2)
            ->create()
            ->each(function(EventCustom $eventCustom) {
                $eventCustom
                    ->eventCustomTimes()
                    ->saveMany(factory(EventCustomTime::class, 5)->create());
        });
    }
}
