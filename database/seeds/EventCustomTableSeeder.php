<?php

use App\Event;
use App\EventCustom;
use Illuminate\Database\Seeder;

class EventCustomTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Event::class, 2)
            ->create()
            ->each(function(Event $event) {
                $event
                    ->eventCustoms()
                    ->saveMany(factory(EventCustom::class, 5)->create());
        });
    }
}
