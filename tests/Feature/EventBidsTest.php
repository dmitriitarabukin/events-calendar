<?php

namespace Tests\Feature;

use App\EventBid;
use App\EventCustom;
use App\EventTime;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class EventBidsTest extends TestCase
{
    use RefreshDatabase;

    /** @var Collection */
    private $eventBids;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed();

        $this->eventBids = EventBid::all();
    }

    /** @test */
    public function could_get_all_event_bids()
    {
        $response = $this->get(route('event_bids.index'));

        $response
            ->assertStatus(200)
            ->assertJson($this->eventBids->toArray());
    }

    /** @test */
    public function could_get_a_single_event_bid()
    {
        $eventBid = $this->eventBids->random();

        $response = $this->get(route('event_bids.show', $eventBid->id));

        $response
            ->assertStatus(200)
            ->assertJson($eventBid->toArray());
    }

    /** @test */
    public function trying_to_get_non_existent_event_bid_will_return_json_error()
    {
        $response = $this->get(route('event_bids.show', 99999));

        $response
            ->assertStatus(404)
            ->assertJson(['Error' => 'Could not find specified resource.']);
    }

    /** @test */
    public function could_store_event_bid_when_event_time_is_available()
    {
        $event = $this->eventBids->random()->event;
        $eventTime = $event->eventTimes->random();
        $eventBid = factory(EventBid::class)->raw([
            'event_id' => $event->id,
            'date' => Carbon::now()->next((int)$eventTime->week_day)->format('Y-m-d'),
            'time' => $eventTime->time,
        ]);

        // Delete all rows to prevent collisions
        EventBid::truncate();

        $response = $this->post(route('events.event_bids.store', $event->id), $eventBid);

        $this->assertDatabaseHas('event_bids', $response->decodeResponseJson());

        $response
            ->assertStatus(201)
            ->assertJson($eventBid);
    }

    /** @test */
    public function could_store_event_bid_when_event_custom_is_available()
    {
        $event = $this->eventBids->random()->event;
        $eventCustom = $event->eventCustoms->random();
        $eventBid = factory(EventBid::class)->raw([
            'event_id' => $event->id,
            'date' => $eventCustom->date,
            'time' => $eventCustom->eventCustomTimes->random()->time,
        ]);

        // Delete all rows to prevent collisions
        EventBid::truncate();

        $response = $this->post(route('events.event_bids.store', $event->id), $eventBid);

        $this->assertDatabaseHas('event_bids', $response->decodeResponseJson());

        $response
            ->assertStatus(201)
            ->assertJson($eventBid);
    }

    /** @test */
    public function trying_to_store_event_bid_which_has_already_been_bidden_will_return_json_error()
    {
        $eventBid = $this->eventBids->random();

        $response = $this->post(route('events.event_bids.store', $eventBid->event->id), $eventBid->toArray());

        $response
            ->assertStatus(422)
            ->assertJson(\json_decode('{"Error":["Event has already been bidden."]}', true));
    }

    /** @test */
    public function trying_to_store_event_bid_for_unavailable_date_and_time_will_return_json_error()
    {
        EventTime::truncate();
        EventCustom::truncate();

        $eventBid = factory(EventBid::class)->make();

        $response = $this->post(route('events.event_bids.store', $eventBid->event->id), $eventBid->toArray());

        $response
            ->assertStatus(422)
            ->assertJson(\json_decode('{"Error":"Chosen date and time are not available for bidding: ' . $eventBid->date . ' ' . $eventBid->time . '."}', true));
    }

    /**
     * @test
     * @dataProvider invalid_data_provider_for_storing_event_bid
     * @param string $field
     * @param string $value
     * @param string $error
     */
    public function trying_to_store_event_bid_passing_invalid_data_will_return_json_error(string $field, string $value, string $error)
    {
        $eventId = $this->eventBids->random()->event->id;
        $eventBid = factory(EventBid::class)->raw([
            'event_id' => $eventId,
            $field => $value,
        ]);

        $response = $this->post(route('events.event_bids.store', $eventId), $eventBid);

        $response
            ->assertStatus(422)
            ->assertJson(\json_decode($error, true));
    }

    public function invalid_data_provider_for_storing_event_bid(): array
    {
        return [
            [
                'date',
                Carbon::now()->subWeek()->format('Y-m-d'),
                '{"Error":["The date must be a date after or equal to ' . Carbon::now('Europe/Moscow')->format('Y-m-d') . '."]}',
            ],
            [
                'date',
                Carbon::now()->addWeek()->format('Y-M-d'),
                '{"Error":["The date does not match the format Y-m-d."]}',
            ],
            [
                'time',
                Carbon::now()->addDay()->format('H:i:s'),
                '{"Error":["The time does not match the format H:i."]}',
            ],
            [
                'name',
                '',
                '{"Error":["The name field is required."]}',
            ],
            [
                'contact',
                '',
                '{"Error":["The contact field is required."]}',
            ],
        ];
    }

    /** @test */
    public function could_update_event_bid()
    {
        $eventBid = $this->eventBids->random();
        $eventTime = $eventBid->event->eventTimes()->first();
        $payload = [
            'date' => Carbon::now()->addMonth()->next((int)$eventTime->week_day)->format('Y-m-d'),
            'time' => $eventTime->time,
        ];

        $response = $this->put(route('event_bids.update', $eventBid->id), $payload);

        $response
            ->assertStatus(200)
            ->assertJson($payload);
    }

    /**
     * @test
     * @dataProvider invalid_data_provider_for_updating_event_bid
     * @param string $field
     * @param string $value
     * @param string $error
     */
    public function trying_to_update_event_bid_passing_invalid_data_will_return_json_error(string $field, string $value, string $error)
    {
        $eventBid = $this->eventBids->random();
        $payload = [$field => $value];

        $response = $this->put(route('event_bids.update', $eventBid->id), $payload);

        $response
            ->assertStatus(422)
            ->assertJson(\json_decode($error, true));
    }

    public function invalid_data_provider_for_updating_event_bid(): array
    {
        return [
            [
                'date',
                Carbon::now()->subWeek()->format('Y-m-d'),
                '{"Error":["The date must be a date after or equal to ' . Carbon::now('Europe/Moscow')->format('Y-m-d') . '."]}',
            ],
            [
                'date',
                Carbon::now()->addWeek()->format('Y-M-d'),
                '{"Error":["The date does not match the format Y-m-d."]}',
            ],
            [
                'time',
                Carbon::now()->addDay()->format('H:i:s'),
                '{"Error":["The time does not match the format H:i."]}',
            ],
        ];
    }

    /** @test */
    public function trying_to_update_event_bid_which_has_already_been_bidden_will_return_json_error()
    {
        $eventBid = $this->eventBids->random();
        $payload = [
            'event_id' => $eventBid->event_id,
            'date' => $eventBid->date,
            'time' => $eventBid->time,
        ];

        $response = $this->put(route('event_bids.update', $eventBid->event->id), $payload);

        $response
            ->assertStatus(422)
            ->assertJson(\json_decode('{"Error":["Identical event bid already exists."]}', true));
    }

    /** @test */
    public function could_destroy_event_bid()
    {
        $eventBid = $this->eventBids->random();

        $response = $this->delete(route('event_bids.destroy', $eventBid->id));

        $this->assertDeleted($eventBid);

        $response->assertStatus(204);
    }
}
