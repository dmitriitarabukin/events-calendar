<?php

namespace Tests\Feature;

use App\Event;
use App\EventCustom;
use App\EventTime;
use EventCustomTableSeeder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class EventsCustomTest extends TestCase
{
    use RefreshDatabase;

    /** @var Collection */
    private $eventCustoms;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed(EventCustomTableSeeder::class);

        $this->eventCustoms = EventCustom::all();
    }

    /** @test */
    public function could_get_all_event_customs()
    {
        $response = $this->get(route('event_custom.index'));

        $response
            ->assertStatus(200)
            ->assertJson($this->eventCustoms->toArray());
    }

    /** @test */
    public function could_get_a_single_event_custom()
    {
        $eventCustom = $this->eventCustoms->random();

        $response = $this->get(route('event_custom.show', $eventCustom->id));

        $response
            ->assertStatus(200)
            ->assertJson($eventCustom->toArray());
    }

    /** @test */
    public function trying_to_get_non_existent_event_custom_will_return_json_error()
    {
        $response = $this->get(route('event_custom.show', 99999));

        $response
            ->assertStatus(404)
            ->assertJson(['Error' => 'Could not find specified resource.']);
    }

    /** @test */
    public function could_store_event_custom()
    {
        $eventId = $this->eventCustoms->random()->event->id;
        $eventCustom = factory(EventCustom::class)->raw(['event_id' => $eventId]);

        $response = $this->post(route('events.event_custom.store', $eventId), $eventCustom);

        $this->assertDatabaseHas('event_customs', $response->decodeResponseJson());

        $response
            ->assertStatus(201)
            ->assertJson($eventCustom);
    }

    /** @test */
    public function trying_to_store_event_custom_with_already_existing_combination_of_date_and_event_id_will_return_json_error()
    {
        $eventCustom = $this->eventCustoms->random();

        $response = $this->post(route('events.event_custom.store', $eventCustom->event->id), $eventCustom->toArray());

        $response
            ->assertStatus(422)
            ->assertJson(\json_decode('{"Error":["EventCustom with the same event_id and date has already been stored."]}', true));
    }

    /**
     * @test
     * @dataProvider invalid_data_provider_for_storing_event_custom
     * @param string $date
     * @param string $error
     */
    public function trying_to_store_event_custom_passing_invalid_data_will_return_json_error(string $date, string $error)
    {
        $eventId = $this->eventCustoms->random()->event->id;
        $eventCustom = factory(EventCustom::class)->raw([
            'event_id' => $eventId,
            'date' => $date,
        ]);

        $response = $this->post(route('events.event_custom.store', $eventId), $eventCustom);

        $response
            ->assertStatus(422)
            ->assertJson(\json_decode($error, true));
    }

    public function invalid_data_provider_for_storing_event_custom(): array
    {
        return [
           [
               Carbon::now()->subDay()->format('Y-m-d'),
               '{"Error":["The date must be a date after or equal to ' . Carbon::now('Europe/Moscow')->format('Y-m-d') . '."]}',
           ],
           [
               Carbon::now()->addDay()->format('Y-M-D'),
               '{"Error":["The date does not match the format Y-m-d."]}',
           ],
        ];
    }

    /** @test */
    public function trying_to_store_event_custom_colliding_with_event_times_will_return_json_error()
    {
        $event = Event::inRandomOrder()->first();
        $eventTime = $event->eventTimes()->saveMany(factory(EventTime::class, 14)->make(['week_day' => '0']))->random();
        $eventCustom = factory(EventCustom::class)->raw([
            'event_id' => $event->id,
            'date' => Carbon::today()->next((int)$eventTime->week_day)->format('Y-m-d'),
        ]);

        $response = $this->post(route('events.event_custom.store', $event->id), $eventCustom);

        $response
            ->assertStatus(422)
            ->assertJson(\json_decode('{"Error":"Chosen date is not available: ' . $eventCustom['date'] . '."}', true));
    }

    /** @test */
    public function could_update_event_custom()
    {
        $eventCustom = $this->eventCustoms->random();
        $payload = ['date' => Carbon::now()->addYear()->format('Y-m-d')];

        $response = $this->put(route('event_custom.update', $eventCustom->id), $payload);

        $response
            ->assertStatus(200)
            ->assertJson($payload);
    }

    /** @test */
    public function trying_to_update_event_custom_with_already_existing_combination_of_date_and_event_id_will_return_json_error()
    {
        $eventCustom = $this->eventCustoms->random();
        $payload = [
            'date' => $eventCustom->date,
            'event_id' => $eventCustom->event_id,
        ];

        $response = $this->put(route('event_custom.update', $eventCustom->id), $payload);

        $response
            ->assertStatus(422)
            ->assertJson(\json_decode('{"Error":["EventCustom with the same event_id and date already exists."]}', true));
    }

    /** @test */
    public function trying_to_update_event_custom_colliding_with_event_times_will_return_json_error()
    {
        $eventCustom = $this->eventCustoms->random();
        $eventTime = $eventCustom->event->eventTimes()->saveMany(factory(EventTime::class, 14)->make(['week_day' => '0']))->random();
        $payload = [
            'event_id' => $eventCustom->event->id,
            'date' => Carbon::today()->next((int)$eventTime->week_day)->format('Y-m-d'),
        ];

        $response = $this->put(route('event_custom.update', $eventCustom->id), $payload);

        $response
            ->assertStatus(422)
            ->assertJson(\json_decode('{"Error":"Event time already exists for this date: ' . $payload['date'] . '."}', true));
    }

    /** @test */
    public function could_destroy_event_custom()
    {
        $eventCustom = $this->eventCustoms->random();

        $response = $this->delete(route('event_custom.destroy', $eventCustom->id));

        $this->assertDeleted($eventCustom);

        $response->assertStatus(204);
    }
}
