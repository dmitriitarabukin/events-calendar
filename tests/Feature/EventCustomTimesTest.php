<?php

namespace Tests\Feature;

use App\Event;
use App\EventCustom;
use App\EventCustomTime;
use App\EventTime;
use EventCustomTimesTableSeeder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class EventsCustomTimesTest extends TestCase
{
    use RefreshDatabase;

    /** @var Collection */
    private $eventCustomTimes;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed(EventCustomTimesTableSeeder::class);

        $this->eventCustomTimes = EventCustomTime::all();
    }

    /** @test */
    public function could_get_all_event_custom_times()
    {
        $response = $this->get(route('event_custom_times.index'));

        $response
            ->assertStatus(200)
            ->assertJson($this->eventCustomTimes->toArray());
    }

    /** @test */
    public function could_get_a_single_event_custom_time()
    {
        $eventCustomTime = $this->eventCustomTimes->random();

        $response = $this->get(route('event_custom_times.show', $eventCustomTime->id));

        $response
            ->assertStatus(200)
            ->assertJson($eventCustomTime->toArray());
    }

    /** @test */
    public function trying_to_get_non_existent_event_custom_time_will_return_json_error()
    {
        $response = $this->get(route('event_custom_times.show', 99999));

        $response
            ->assertStatus(404)
            ->assertJson(['Error' => 'Could not find specified resource.']);
    }

    /** @test */
    public function could_store_event_custom_time()
    {
        $eventCustomId = $this->eventCustomTimes->random()->eventCustom->id;
        $eventCustomTime = factory(EventCustomTime::class)->raw(['event_custom_id' => $eventCustomId]);

        $response = $this->post(route('event_custom.event_custom_times.store', $eventCustomId), $eventCustomTime);

        $this->assertDatabaseHas('event_custom_times', $response->decodeResponseJson());

        $response
            ->assertStatus(201)
            ->assertJson($eventCustomTime);
    }

    /** @test */
    public function trying_to_store_event_custom_time_with_already_existing_combination_of_event_custom_id_and_time_will_return_json_error()
    {
        $eventCustomTime = $this->eventCustomTimes->random();

        $response = $this->post(route('event_custom.event_custom_times.store', $eventCustomTime->eventCustom->id), $eventCustomTime->toArray());

        $response
            ->assertStatus(422)
            ->assertJson(\json_decode('{"Error":["EventCustomTime with the same event_custom_id and time has already been stored."]}', true));
    }

    /** @test */
    public function trying_to_store_event_custom_time_passing_invalid_time_format_will_return_json_error()
    {
        $eventCustomId = $this->eventCustomTimes->random()->eventCustom->id;
        $eventCustomTime = factory(EventCustomTime::class)->raw([
            'event_custom_id' => $eventCustomId,
            'time' => Carbon::now()->format('H:i:s'),
        ]);

        $response = $this->post(route('event_custom.event_custom_times.store', $eventCustomId), $eventCustomTime);

        $response
            ->assertStatus(422)
            ->assertJson(\json_decode('{"Error":["The time does not match the format H:i."]}', true));
    }

    /** @test */
    public function trying_to_store_event_custom_time_colliding_with_event_times_will_return_json_error()
    {
        $event = Event::inRandomOrder()->first();
        $eventTime = $event->eventTimes()->create(factory(EventTime::class)->make()->toArray());
        $eventCustom = $event->eventCustoms()->create(factory(EventCustom::class)->raw([
            'event_id' => $event->id,
            'date' => Carbon::today()->next((int)$eventTime->week_day)->format('Y-m-d'),
            ]));
        $eventCustomTime = factory(EventCustomTime::class)->raw([
            'event_custom_id' => $eventCustom->id,
            'time' => $eventTime->time,
        ]);

        $response = $this->post(route('event_custom.event_custom_times.store', $eventCustom->id), $eventCustomTime);

        $response
            ->assertStatus(422)
            ->assertJson(\json_decode('{"Error":"Event time already exists for this date and time: ' . $eventCustom->date . ' ' . $eventCustomTime['time'] . '."}', true));
    }

    /** @test */
    public function could_update_event_custom_time()
    {
        $eventCustomTimes = $this->eventCustomTimes->random();
        $payload = ['time' => '21:00'];

        $response = $this->put(route('event_custom_times.update', $eventCustomTimes->id), $payload);

        $response
            ->assertStatus(200)
            ->assertJson($payload);
    }

    /** @test */
    public function trying_to_update_event_custom_time_with_already_existing_combination_of_event_custom_id_and_time_will_return_json_error()
    {
        $eventCustomTime = $this->eventCustomTimes->random();
        $payload = [
            'event_custom_id' => $eventCustomTime->event_custom_id,
            'time' => $eventCustomTime->time,
        ];

        $response = $this->put(route('event_custom_times.update', $eventCustomTime->id), $payload);

        $response
            ->assertStatus(422)
            ->assertJson(\json_decode('{"Error":["EventCustomTime with the same event_custom_id and time already exists."]}', true));
    }

    /** @test */
    public function trying_to_update_event_custom_time_colliding_with_event_times_will_return_json_error()
    {
        $event = Event::inRandomOrder()->first();
        $eventTime = $event->eventTimes()->create(factory(EventTime::class)->raw());
        $eventCustom = $event->eventCustoms()->create(factory(EventCustom::class)->make(['date' => Carbon::now()->addYear()->next((int)$eventTime->week_day)->format('Y-m-d')])->toArray());
        $eventCustomTime = $eventCustom->eventCustomTimes()->create(factory(EventCustomTime::class)->raw());
        $payload = [
            'event_custom_id' => $eventCustom->id,
            'time' => $eventTime->time,
        ];

        $response = $this->put(route('event_custom_times.update', $eventCustomTime->id), $payload);

        $response
            ->assertStatus(422)
            ->assertJson(\json_decode('{"Error":"Event time already exists for this date and time: ' . $eventCustom->date . ' ' . $eventTime->time . '."}', true));
    }

    /** @test */
    public function could_destroy_event_custom_time()
    {
        $eventCustomTime = $this->eventCustomTimes->random();

        $response = $this->delete(route('event_custom_times.destroy', $eventCustomTime->id));

        $this->assertDeleted($eventCustomTime);

        $response->assertStatus(204);
    }
}
