<?php

namespace Tests\Feature;

use App\EventTime;
use EventTimesTableSeeder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EventsTimesTest extends TestCase
{
    use RefreshDatabase;

    /** @var Collection */
    private $eventTimes;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed(EventTimesTableSeeder::class);

        $this->eventTimes = EventTime::all();
    }

    /** @test */
    public function could_get_all_event_times()
    {
        $response = $this->get(route('event_times.index'));

        $response
            ->assertStatus(200)
            ->assertJson($this->eventTimes->toArray());
    }

    /** @test */
    public function could_get_a_single_event_time()
    {
        $eventTime = $this->eventTimes->random();

        $response = $this->get(route('event_times.show', $eventTime->id));

        $response
            ->assertStatus(200)
            ->assertJson($eventTime->toArray());
    }

    /** @test */
    public function trying_to_get_non_existent_event_time_will_return_json_error()
    {
        $response = $this->get(route('event_times.show', 99999));

        $response
            ->assertStatus(404)
            ->assertJson(['Error' => 'Could not find specified resource.']);
    }

    /** @test */
    public function could_store_event_time()
    {
        $eventId = $this->eventTimes->random()->event->id;
        $eventTime = factory(EventTime::class)->raw(['event_id' => $eventId]);

        $response = $this->post(route('events.event_times.store', $eventId), $eventTime);

        $this->assertDatabaseHas('event_times', $response->decodeResponseJson());

        $response
            ->assertStatus(201)
            ->assertJson($eventTime);
    }

    /** @test */
    public function trying_to_store_event_time_with_already_existing_combination_of_week_day_and_time_and_event_id_will_return_json_error()
    {
        $eventTime = $this->eventTimes->random();

        $response = $this->post(route('events.event_times.store', $eventTime->event->id), $eventTime->toArray());

        $response
            ->assertStatus(422)
            ->assertJson(\json_decode('{"Error":["EventTime with the same event_id, week_day and time has already been stored."]}', true));
    }

    /** @test */
    public function trying_to_store_event_time_with_invalid_time_will_return_json_error()
    {
        $eventTime = $this->eventTimes->random();
        $eventTime->time = '03:00';

        $response = $this->post(route('events.event_times.store', $eventTime->event->id), $eventTime->toArray());

        $response
            ->assertStatus(422)
            ->assertJson(\json_decode('{"Error":["The time is not in a valid range 08:00-21:00 or minutes are invalid."]}', true));
    }

    /** @test */
    public function could_update_event_time()
    {
        $eventTime = $this->eventTimes->random();
        $payload = [
            'week_day' => \rand(1, 7),
            'time' => '21:00',
        ];

        $response = $this->put(route('event_times.update', $eventTime->id), $payload);

        $response
            ->assertStatus(200)
            ->assertJson($payload);
    }

    /** @test */
    public function trying_to_update_event_time_with_already_existing_combination_of_week_day_and_time_and_event_id_will_return_json_error()
    {
        $eventTime = $this->eventTimes->random();
        $payload = [
            'week_day' => $eventTime->week_day,
            'time' => $eventTime->time,
            'event_id' => $eventTime->event_id,
        ];

        $response = $this->put(route('event_times.update', $eventTime->event->id), $payload);

        $response
            ->assertStatus(422)
            ->assertJson(\json_decode('{"Error":["EventTime with the same event_id, week_day and time already exists."]}', true));
    }

    /** @test */
    public function could_destroy_event_time()
    {
        $eventTime = $this->eventTimes->random();

        $response = $this->delete(route('event_times.destroy', $eventTime->id));

        $this->assertDeleted($eventTime);

        $response->assertStatus(204);
    }
}
