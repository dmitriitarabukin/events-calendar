<?php

namespace Tests\Feature;

use App\Event;
use App\EventBid;
use App\EventCustom;
use App\EventCustomTime;
use App\EventTime;
use App\Services\EventMapService;
use App\TimeMapItem;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Tests\TestCase;

class EventMapServiceTest extends TestCase
{
    use RefreshDatabase;

    private $event = [];

    /** @var Collection */
    private $eventBids;

    public function setUp(): void
    {
        parent::setUp();

        $this->event = factory(Event::class)->raw();
        $this->event['id'] = $this->post(route('events.store'), $this->event)->decodeResponseJson()['id'];

        $eventTime1 = factory(EventTime::class)->raw(['event_id' => $this->event['id']]);
        $eventTime1 = $this->post(route('events.event_times.store', $this->event['id']), $eventTime1)->decodeResponseJson();

        $eventBid1 = factory(EventBid::class)->raw([
            'event_id' => $this->event['id'],
            'date' => Carbon::now()->next((int)$eventTime1['week_day'])->format('Y-m-d'),
            'time' => $eventTime1['time'],
        ]);
        $this->post(route('events.event_bids.store', $this->event['id']), $eventBid1);

        $eventTime2 = factory(EventTime::class)->raw(['event_id' => $this->event['id']]);
        $eventTime2 = $this->post(route('events.event_times.store', $this->event['id']), $eventTime2)->decodeResponseJson();

        $eventBid2 = factory(EventBid::class)->raw([
            'event_id' => $this->event['id'],
            'date' => Carbon::now()->next((int)$eventTime2['week_day'])->format('Y-m-d'),
            'time' => $eventTime2['time'],
        ]);
        $this->post(route('events.event_bids.store', $this->event['id']), $eventBid2);

        $eventCustom = factory(EventCustom::class)->raw([
            'event_id' => $this->event['id'],
            'date' => Carbon::now()->next(2)->format('Y-m-d'),
        ]);
        $eventCustomId = $this->post(route('events.event_custom.store', $this->event['id']), $eventCustom)->decodeResponseJson()['id'];

        $eventCustomTime = factory(EventCustomTime::class)->raw([
            'event_custom_id' => $eventCustomId,
            'time' => '13:00',
        ]);
        $this->post(route('event_custom.event_custom_times.store', $eventCustomId), $eventCustomTime);

        $eventBid3 = factory(EventBid::class)->raw([
            'event_id' => $this->event['id'],
            'date' => $eventCustom['date'],
            'time' => $eventCustomTime['time'],
        ]);
        $this->post(route('events.event_bids.store', $this->event['id']), $eventBid3)->decodeResponseJson();

        $this->eventBids = new Collection([$eventBid1, $eventBid2, $eventBid3]);
    }

    /** @test */
    public function if_there_are_bidden_events_calling_findTimeMap_should_return_correct_bidden_events()
    {
        $eventMapService = new EventMapService(new Event());
        $timeMap = $eventMapService->findTimeMap(
            Carbon::now()->format('Y-m-d'),
            $this->event['id'],
            Carbon::now()->addMonth()->format('Y-m-d')
        );
        $biddenTimes = $timeMap->where('isFree', false);

        $this->eventBids->each(function(array $eventBid) use ($biddenTimes) {
            $this->assertTrue($biddenTimes->contains(new TimeMapItem([
                'date' => $eventBid['date'],
                'time' => $eventBid['time'],
                'eventName' => $this->event['name'],
                'isFree' => false,
            ])));
        });
    }

    /** @test */
    public function if_there_are_bidden_events_calling_findMapByDate_should_return_correct_bidden_events()
    {
        $eventMapService = App::make(EventMapService::class);
        $timeMap = $eventMapService->findMapByDate($this->eventBids->first()['date']);
        $biddenTime = $timeMap->where('isFree', false)->first();

        $this->assertNotEmpty(
            $this->eventBids
                ->where('date', $biddenTime->date)
                ->where('time', $biddenTime->time)
                ->first()
        );
    }
}
