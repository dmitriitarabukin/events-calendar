<?php

namespace Tests\Feature;

use App\Event;
use EventsTableSeeder;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EventsTest extends TestCase
{
    use RefreshDatabase;

    /** @var Collection */
    private $events;

    /** @var Generator */
    private $faker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed(EventsTableSeeder::class);

        $this->events = Event::all();
        $this->faker = Factory::create();
    }

    /** @test */
    public function could_get_all_events()
    {
        $response = $this->get(route('events.index'));

        $response
            ->assertStatus(200)
            ->assertJson($this->events->toArray());
    }

    /** @test */
    public function could_get_a_single_event()
    {
        $event = $this->events->random();

        $response = $this->get(route('events.show', $event->id));

        $response
            ->assertStatus(200)
            ->assertJson($event->toArray());
    }

    /** @test */
    public function trying_to_get_non_existent_event_will_return_json_error()
    {
        $response = $this->get(route('events.show', 99999));

        $response
            ->assertStatus(404)
            ->assertJson(['Error' => 'Could not find specified resource.']);
    }

    /** @test */
    public function could_store_event()
    {
        $event = factory(Event::class)->raw();

        $response = $this->post(route('events.store'), $event);

        $this->assertDatabaseHas('events', $response->decodeResponseJson());

        $response
            ->assertStatus(201)
            ->assertJson($event);
    }

    /** @test */
    public function trying_to_store_event_with_already_existing_name_will_return_json_error()
    {
        $event= $this->events->random();

        $response = $this->post(route('events.store', $event->id), $event->toArray());

        $response
            ->assertStatus(422)
            ->assertJson(\json_decode('{"Error":["Event with the same name has already been stored."]}', true));
    }

    /** @test */
    public function could_update_event()
    {
        $event = $this->events->random();
        $payload = [
            'name' => $this->faker->sentence,
            'description' => $this->faker->paragraph,
        ];

        $response = $this->put(route('events.update', $event->id), $payload);

        $response
            ->assertStatus(200)
            ->assertJson($payload);
    }

    /** @test */
    public function trying_to_update_event_with_already_existing_name_will_return_json_error()
    {
        $event= $this->events->random();
        $payload = ['name' => $event->name];

        $response = $this->put(route('events.update', $event->id), $payload);

        $response
            ->assertStatus(422)
            ->assertJson(\json_decode('{"Error":["Event with the same name already exists."]}', true));
    }

    /** @test */
    public function could_destroy_event()
    {
        $event = $this->events->random();

        $response = $this->delete(route('events.destroy', $event->id));

        $this->assertDeleted($event);

        $response->assertStatus(204);
    }
}
