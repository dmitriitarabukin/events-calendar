<?php

namespace Tests\Unit;

use App\Event;
use App\EventBid;
use App\Exceptions\EventMapServiceException;
use App\Services\EventMapService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Tests\TestCase;

class EventMapServiceTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var EventMapService
     */
    private $eventMapService;

    /** @test */
    public function calling_findTimeMap_should_return_time_map()
    {
        $this->seed();

        $this->eventMapService = App::make(EventMapService::class);

        $timeMap = $this->eventMapService->findTimeMap(
            Carbon::now()->format('Y-m-d'),
            Event::inRandomOrder()->first()->id,
            Carbon::now()->addMonth()->format('Y-m-d')
        );

        $this->assertInstanceOf(Collection::class, $timeMap);
    }

    /** @test */
    public function calling_findTimeMap_without_maxDate_should_be_possible()
    {
        $this->seed();

        $this->eventMapService = App::make(EventMapService::class);

        $timeMap = $this->eventMapService->findTimeMap(
            Carbon::now()->format('Y-m-d'),
            Event::inRandomOrder()->first()->id
        );

        $this->assertInstanceOf(Collection::class, $timeMap);
    }

    /** @test */
    public function calling_findTimeMap_should_return_correct_time_map_structure()
    {
        $this->seed();

        $this->eventMapService = App::make(EventMapService::class);

        $timeMap = $this->eventMapService->findTimeMap(
            Carbon::now()->format('Y-m-d'),
            Event::inRandomOrder()->first()->id,
            Carbon::now()->addMonth()->format('Y-m-d')
        );

        (new TestResponse(new Response()))
            ->assertJsonStructure(['*' => [
                'date',
                'time',
                'eventName',
                'isFree',
            ]], $timeMap->toArray());
    }

    /** @test */
    public function if_there_are_bidden_events_calling_findTimeMap_should_return_time_map_with_bidden_items()
    {
        $this->seed();

        $this->eventMapService = App::make(EventMapService::class);

        $timeMap = $this->eventMapService->findTimeMap(
            Carbon::now()->format('Y-m-d'),
            Event::inRandomOrder()->first()->id,
            Carbon::now()->addMonth()->format('Y-m-d')
        );

        $this->assertCount(2, $timeMap->where('isFree', false));
    }

    /** @test */
    public function calling_findTimeMap_should_call_findEventWithRelationsById_and_eventTimes_and_eventCustoms_and_each_and_eventBids_and_get_on_event()
    {
        $mockedEvent = $this->mock(Event::class)->makePartial();
        $mockedEvent->id = rand(1, 10);

        $mockedEvent
            ->shouldReceive('findEventWithRelationsById')
            ->once()
            ->with($mockedEvent->id, ['eventTimes', 'eventCustoms.eventCustomTimes', 'eventBids'])
            ->andReturn($mockedEvent);
        $mockedEvent
            ->shouldReceive('eventTimes')
            ->once()
            ->andReturn($mockedEvent);
        $mockedEvent
            ->shouldReceive('eventCustoms')
            ->once()
            ->andReturn($mockedEvent);
        $mockedEvent
            ->shouldReceive('each')
            ->twice()
            ->andReturn(true);
        $mockedEvent
            ->shouldReceive('eventBids->get')
            ->once()
            ->andReturn(new Collection());


        $this->eventMapService = App::make(
            EventMapService::class,
            [$mockedEvent]
        );

        $this->eventMapService->findTimeMap(
            Carbon::now()->format('Y-m-d'),
            $mockedEvent->id,
            Carbon::now()->addMonth()->format('Y-m-d')
        );
    }

    /** @test */
    public function calling_findTimeMap_passing_date_less_than_today_will_throw_exception()
    {
        $this->expectException(EventMapServiceException::class);
        $this->expectExceptionMessage('date should be more than or equal to now.');

        $this->setup_mocked_event_map_service();

        $this->eventMapService->findTimeMap(
            Carbon::now()->subDay()->format('Y-m-d'),
            rand(1, 10),
            Carbon::now()->addMonth()->format('Y-m-d')
        );
    }

    /**
     * @test
     * @dataProvider invalid_maxDate_provider
     * @param string $maxDate
     * @throws EventMapServiceException
     */
    public function calling_findTimeMap_passing_maxDate_less_than_or_equal_to_date_will_throw_exception(string $maxDate)
    {
        $this->expectException(EventMapServiceException::class);
        $this->expectExceptionMessage('maxDate should be greater than date.');

        $this->setup_mocked_event_map_service();

        $this->eventMapService->findTimeMap(
            Carbon::now()->format('Y-m-d'),
            rand(1, 10),
            $maxDate
        );
    }

    public function invalid_maxDate_provider(): array
    {
        return [
            [Carbon::yesterday()->format('Y-m-d')],
            [Carbon::now()->format('Y-m-d')],
        ];
    }


    /** @test */
    public function calling_findTimeMap_with_wrong_event_id_should_return_json_error()
    {
        $eventId = rand(1, 999);

        $this->expectException(EventMapServiceException::class);
        $this->expectExceptionMessage('Event with id ' . $eventId . ' was not found.');

        $this->eventMapService = App::make(EventMapService::class);

        $this->eventMapService->findTimeMap(
            Carbon::now()->format('Y-m-d'),
            $eventId,
            Carbon::now()->addMonth()->format('Y-m-d')
        );
    }

    /** @test */
    public function calling_findMapByDate_should_return_time_map()
    {
        $this->seed();

        $this->eventMapService = App::make(EventMapService::class);

        $timeMap = $this->eventMapService->findMapByDate(Carbon::tomorrow()->format('Y-m-d'));

        $this->assertInstanceOf(Collection::class, $timeMap);
    }

    /** @test */
    public function calling_findMapByDate_passing_date_less_than_today_will_throw_exception()
    {
        $this->expectException(EventMapServiceException::class);
        $this->expectExceptionMessage('date should be more than or equal to now.');

        $this->setup_mocked_event_map_service();

        $this->eventMapService->findMapByDate(Carbon::yesterday()->format('Y-m-d'));
    }

    /** @test */
    public function calling_findMapByDate_should_call_findAllEventsWithRelations_on_event()
    {
        $mockedEvent = $this->mock(Event::class)->makePartial();
        $mockedEvent->id = rand(1, 10);

        $mockedEvent
            ->shouldReceive('findAllEventsWithRelations')
            ->once()
            ->with(['eventTimes', 'eventCustoms.eventCustomTimes', 'eventBids'])
            ->andReturn(new \Illuminate\Database\Eloquent\Collection());

        $this->eventMapService = App::make(
            EventMapService::class,
            [$mockedEvent]
        );

        $this->eventMapService->findMapByDate(Carbon::tomorrow()->format('Y-m-d'));
    }

    /** @test */
    public function calling_findMapByDate_should_return_correct_time_map_structure()
    {
        $this->seed();

        $this->eventMapService = App::make(EventMapService::class);

        $timeMap = $this->eventMapService->findMapByDate(Carbon::tomorrow()->format('Y-m-d'));

        (new TestResponse(new Response()))
            ->assertJsonStructure(['*' => [
                'date',
                'time',
                'eventName',
                'isFree',
            ]], $timeMap->toArray());
    }

    /** @test */
    public function if_there_are_bidden_events_calling_findMapByDate_should_return_time_map_with_bidden_items()
    {
        $this->seed();

        $this->eventMapService = App::make(EventMapService::class);

        $timeMap = $this->eventMapService->findMapByDate(EventBid::inRandomOrder()->first()->date);

        $this->assertCount(1, $timeMap->where('isFree', false));
    }

    private function setup_mocked_event_map_service()
    {
        $mockedEvent = $this->mock(Event::class)->makePartial();
        $mockedEvent->id = rand(1, 10);

        $this->eventMapService = App::make(
            EventMapService::class,
            [$mockedEvent]
        );

    }
}
