<?php

namespace Tests\Unit;

use App\Event;
use App\EventCustom;
use App\EventCustomTime;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EventCustomTest extends TestCase
{
    use RefreshDatabase;

    /** @var EventCustom */
    private $eventCustom;

    public function setUp(): void
    {
        parent::setUp();

        $this->eventCustom = factory(EventCustom::class)->create();
    }

    /** @test */
    public function event_custom_has_event()
    {
        $this->assertInstanceOf(Event::class, $this->eventCustom->event);
    }

    /** @test */
    public function event_custom_can_add_event_custom_time()
    {
        $this->eventCustom->addEventCustomTime(factory(EventCustomTime::class)->raw());

        $this->assertCount(1, $this->eventCustom->eventCustomTimes);
    }
}
