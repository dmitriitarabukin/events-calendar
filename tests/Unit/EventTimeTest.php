<?php

namespace Tests\Unit;

use App\Event;
use App\EventTime;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EventTimeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function event_time_has_event()
    {
        $eventTime = factory(EventTime::class)->create();

        $this->assertInstanceOf(Event::class, $eventTime->event);
    }
}
