<?php

namespace Tests\Unit;

use App\Rules\EventTimeRange;
use Tests\TestCase;

class TimeRangeTest extends TestCase
{
    /** @var EventTimeRange */
    private $timeRange;

    public function setUp(): void
    {
        parent::setUp();

        $this->timeRange = new EventTimeRange;
    }

    /** @test */
    public function passes_when_time_is_in_range()
    {
        $this->assertTrue($this->timeRange->passes('time', '14:00'));
    }

    /** @test */
    public function fails_when_time_is_out_of_range()
    {
        $this->assertFalse($this->timeRange->passes('time', '05:00'));
    }

    /** @test */
    public function fails_when_invalid_minutes_are_passed()
    {
        $this->assertFalse($this->timeRange->passes('time', '16:11'));
    }
}
