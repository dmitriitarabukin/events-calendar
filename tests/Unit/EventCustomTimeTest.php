<?php

namespace Tests\Unit;

use App\EventCustom;
use App\EventCustomTime;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EventCustomTimeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function event_custom_time_has_event_custom()
    {
        $eventCustomTime = factory(EventCustomTime::class)->create();

        $this->assertInstanceOf(EventCustom::class, $eventCustomTime->eventCustom);
    }
}
