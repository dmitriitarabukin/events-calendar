<?php

namespace Tests\Unit;

use App\Event;
use App\EventBid;
use App\EventCustom;
use App\EventTime;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EventTest extends TestCase
{
    use RefreshDatabase;

    /** @var Event */
    private $event;

    public function setUp(): void
    {
        parent::setUp();

        $this->event = factory(Event::class)->create();
    }

    /** @test */
    public function event_has_event_times()
    {
        $this->assertInstanceOf(Collection::class, $this->event->eventTimes);
    }

    /** @test */
    public function event_has_event_customs()
    {
        $this->assertInstanceOf(Collection::class, $this->event->eventCustoms);
    }

    /** @test */
    public function event_has_event_bids()
    {
        $this->assertInstanceOf(Collection::class, $this->event->eventBids);
    }

    /** @test */
    public function event_can_add_event_time()
    {
        $this->event->addEventTime(factory(EventTime::class)->raw(['event_id' => $this->event->id]));

        $this->assertCount(1, $this->event->eventTimes);
    }

    /** @test */
    public function event_can_add_event_custom()
    {
        $this->event->addEventCustom(factory(EventCustom::class)->raw(['event_id' => $this->event->id]));

        $this->assertCount(1, $this->event->eventCustoms);
    }

    /** @test */
    public function event_can_add_event_bid()
    {
        $this->event->addEventBid(factory(EventBid::class)->raw(['event_id' => $this->event->id]));

        $this->assertCount(1, $this->event->eventBids);
    }
}
