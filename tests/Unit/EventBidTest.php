<?php

namespace Tests\Unit;

use App\Event;
use App\EventBid;
use App\EventTime;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class EventBidTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function event_bid_has_event()
    {
        $eventBid = factory(EventBid::class)->create();

        $this->assertInstanceOf(Event::class, $eventBid->event);
    }

    /** @test */
    public function event_bid_can_update_event_bid()
    {
        $event = factory(Event::class)->create();
        $event->eventTimes()->saveMany(factory(EventTime::class, 5)->create());

        $eventTime = $event->eventTimes->random();

        $eventBid = factory(EventBid::class)->create(['event_id' => $event->id]);

        $eventBid->tryUpdateEventBid(factory(EventBid::class)->raw([
            'event_id' => $event->id,
            'date' => Carbon::now()->addMonth()->next((int)$eventTime->week_day)->format('Y-m-d'),
            'time' => $eventTime->time,
        ]));

        $this->assertCount(1, $event->eventBids);
    }
}
