<?php

namespace App;

use App\Exceptions\EventBidException;
use Illuminate\Database\Eloquent\Model;

class EventBid extends Model
{
    protected $fillable = [
        'event_id',
        'date',
        'time',
        'name',
        'contact',
    ];

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    /**
     * @param array $payload
     * @return void
     * @throws EventBidException
     */
    public function tryUpdateEventBid(array $payload): void
    {
        $this->fill($payload);

        if (($this->event->isEventTimeAvailableForBidding($this->toArray()) === false)
            && ($this->event->isEventCustomAvailableForBidding($this->toArray()) === false)) {
            throw new EventBidException('Chosen date and time are not available for bidding: ' . $this->date . ' ' . $this->time . '.');
        }

        $this->update($payload);
    }
}

