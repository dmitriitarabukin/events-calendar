<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Carbon;

class EventTimeRange implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $minTime = Carbon::createFromTimeString('08:00');
        $maxTime = Carbon::createFromTimeString('21:00');
        $eventTime = Carbon::createFromTimeString($value);

        return ($eventTime->minute === 0 && $eventTime->betweenIncluded($minTime, $maxTime) === true);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute is not in a valid range 08:00-21:00 or minutes are invalid.';
    }
}
