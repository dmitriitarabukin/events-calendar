<?php

namespace App\Http\Controllers\API;

use App\Event;
use App\EventTime;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreEventTime;
use App\Http\Requests\UpdateEventTime;
use Illuminate\Http\JsonResponse;

class EventTimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json(EventTime::all(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreEventTime $request
     * @param Event $event
     * @return JsonResponse
     */
    public function store(StoreEventTime $request, Event $event): JsonResponse
    {
        $eventTime = $event->addEventTime($request->all());

        return response()->json($eventTime, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param EventTime $eventTime
     * @return JsonResponse
     */
    public function show(EventTime $eventTime): JsonResponse
    {
        return response()->json($eventTime, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateEventTime $request
     * @param EventTime $eventTime
     * @return JsonResponse
     */
    public function update(UpdateEventTime $request, EventTime $eventTime): JsonResponse
    {
        $eventTime->update($request->all());

        return response()->json($eventTime, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param EventTime $eventTime
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(EventTime $eventTime): JsonResponse
    {
        $eventTime->delete();

        return response()->json(null, 204);
    }
}
