<?php

namespace App\Http\Controllers\API;

use App\EventCustom;
use App\EventCustomTime;
use App\Exceptions\EventCustomException;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreEventCustomTime;
use App\Http\Requests\UpdateEventCustomTime;
use Illuminate\Http\JsonResponse;

class EventCustomTimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json(EventCustomTime::all(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreEventCustomTime $request
     * @param EventCustom $eventCustom
     * @return JsonResponse
     */
    public function store(StoreEventCustomTime $request, EventCustom $eventCustom): JsonResponse
    {
        try {
            $eventCustomTime = $eventCustom->tryAddEventCustomTime($request->all());
        } catch (EventCustomException $exception) {
            return response()->json(['Error' => $exception->getMessage()], 422);
        }

        return response()->json($eventCustomTime, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param EventCustomTime $eventCustomTime
     * @return JsonResponse
     */
    public function show(EventCustomTime $eventCustomTime): JsonResponse
    {
        return response()->json($eventCustomTime, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateEventCustomTime $request
     * @param EventCustomTime $eventCustomTime
     * @return JsonResponse
     */
    public function update(UpdateEventCustomTime $request, EventCustomTime $eventCustomTime): JsonResponse
    {
        try {
            $eventCustomTime->tryUpdateEventCustomTime($request->all());
        } catch(EventCustomException $exception) {
            return response()->json(['Error' => $exception->getMessage()], 422);
        }

        return response()->json($eventCustomTime, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param EventCustomTime $eventCustomTime
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(EventCustomTime $eventCustomTime): JsonResponse
    {
        $eventCustomTime->delete();

        return response()->json(null, 204);
    }
}
