<?php

namespace App\Http\Controllers\API;

use App\Event;
use App\EventBid;
use App\Exceptions\EventBidException;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreEventBid;
use App\Http\Requests\UpdateEventBid;
use Illuminate\Http\JsonResponse;

class EventBidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json(EventBid::all(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreEventBid $request
     * @param Event $event
     * @return JsonResponse
     */
    public function store(StoreEventBid $request, Event $event): JsonResponse
    {
        try {
            $eventBid = $event->tryAddEventBid($request->all());
        } catch(EventBidException $exception) {
            return response()->json(['Error' => $exception->getMessage()], 422);
        }

        return response()->json($eventBid, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param EventBid $eventBid
     * @return JsonResponse
     */
    public function show(EventBid $eventBid): JsonResponse
    {
        return response()->json($eventBid, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateEventBid $request
     * @param EventBid $eventBid
     * @return JsonResponse
     */
    public function update(UpdateEventBid $request, EventBid $eventBid): JsonResponse
    {
        try {
            $eventBid->tryUpdateEventBid($request->all());
        } catch(EventBidException $exception) {
            return response()->json(['Error' => $exception->getMessage()], 422);
        }

        return response()->json($eventBid, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param EventBid $eventBid
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(EventBid $eventBid): JsonResponse
    {
        $eventBid->delete();

        return response()->json(null, 204);
    }
}
