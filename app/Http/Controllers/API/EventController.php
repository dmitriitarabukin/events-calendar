<?php

namespace App\Http\Controllers\API;

use App\Event;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreEvent;
use App\Http\Requests\UpdateEvent;
use Illuminate\Http\JsonResponse;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json(Event::all(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreEvent $request
     * @return JsonResponse
     */
    public function store(StoreEvent $request): JsonResponse
    {
        $event = Event::create($request->all());

        return response()->json($event, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param Event $event
     * @return JsonResponse
     */
    public function show(Event $event): JsonResponse
    {
        return response()->json($event, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateEvent $request
     * @param Event $event
     * @return JsonResponse
     */
    public function update(UpdateEvent $request, Event $event): JsonResponse
    {
        $event->update($request->all());

        return response()->json($event, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Event $event
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(Event $event): JsonResponse
    {
        $event->delete();

        return response()->json(null, 204);
    }
}
