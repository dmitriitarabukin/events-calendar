<?php

namespace App\Http\Controllers\API;

use App\Event;
use App\EventCustom;
use App\Exceptions\EventCustomException;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreEventCustom;
use App\Http\Requests\UpdateEventCustom;
use Illuminate\Http\JsonResponse;

class EventCustomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json(EventCustom::all(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreEventCustom $request
     * @param Event $event
     * @return JsonResponse
     */
    public function store(StoreEventCustom $request, Event $event): JsonResponse
    {
        try {
            $eventCustom = $event->tryAddEventCustom($request->all());
        } catch (EventCustomException $exception) {
            return response()->json(['Error' => $exception->getMessage()], 422);
        }

        return response()->json($eventCustom, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param EventCustom $eventCustom
     * @return JsonResponse
     */
    public function show(EventCustom $eventCustom): JsonResponse
    {
        return response()->json($eventCustom, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateEventCustom $request
     * @param EventCustom $eventCustom
     * @return JsonResponse
     */
    public function update(UpdateEventCustom $request, EventCustom $eventCustom): JsonResponse
    {
        try {
            $eventCustom->tryUpdateEventCustom($request->all());
        } catch (EventCustomException $exception) {
            return response()->json(['Error' => $exception->getMessage()], 422);
        }

        return response()->json($eventCustom, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param EventCustom $eventCustom
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(EventCustom $eventCustom): JsonResponse
    {
        $eventCustom->delete();

        return response()->json(null, 204);
    }
}
