<?php

namespace App\Http\Requests;

use App\EventTime;
use App\Rules\EventTimeRange;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class StoreEventTime extends FormRequest
{
    public function rules(): array
    {
        return [
            'week_day' => [
                'required',
                'between:0,6',
                Rule::unique(EventTime::class)->where(function($query) {
                    return $query
                        ->where('week_day', $this->week_day)
                        ->where('time', $this->time)
                        ->where('event_id', $this->event_id);
                }),
            ],
            'time' => [
                'required',
                'date_format:H:i',
                new EventTimeRange,
            ]
        ];
    }

    public function messages(): array
    {
        return ['week_day.unique' => 'EventTime with the same event_id, week_day and time has already been stored.'];
    }

    /**
     * @param Validator $validator
     * @throws HttpResponseException
     */
    protected function failedValidation(Validator $validator)
    {
        $jsonResponse = response()->json(['Error' => $validator->errors()->all()], 422);

        throw new HttpResponseException($jsonResponse);
    }
}
