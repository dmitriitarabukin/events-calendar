<?php

namespace App\Http\Requests;

use App\EventCustom;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Carbon;
use Illuminate\Validation\Rule;

class UpdateEventCustom extends FormRequest
{
    public function rules(): array
    {
        return [
            'date' => [
                'required',
                'date_format:Y-m-d',
                'after_or_equal:' . Carbon::today()->format('Y-m-d'),
                Rule::unique(EventCustom::class)->where(function($query) {
                    return $query
                        ->where('date', $this->date)
                        ->where('event_id', $this->event_id);
                })->ignore($this->id),
            ]
        ];
    }

    public function messages(): array
    {
        return ['date.unique' => 'EventCustom with the same event_id and date already exists.'];
    }

    /**
     * @param Validator $validator
     * @throws HttpResponseException
     */
    protected function failedValidation(Validator $validator)
    {
        $jsonResponse = response()->json(['Error' => $validator->errors()->all()], 422);

        throw new HttpResponseException($jsonResponse);
    }
}
