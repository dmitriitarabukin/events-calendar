<?php

namespace App\Http\Requests;

use App\EventBid;
use App\Rules\EventTimeRange;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Carbon;
use Illuminate\Validation\Rule;

class StoreEventBid extends FormRequest
{
    public function rules(): array
    {
        return [
            'date' => [
                'required',
                'date_format:Y-m-d',
                'after_or_equal:' . Carbon::today()->format('Y-m-d'),
                Rule::unique(EventBid::class)->where(function($query) {
                    return $query
                        ->where('date', $this->date)
                        ->where('event_id', $this->event_id)
                        ->where('time', $this->time);
                }),
            ],
            'time' => [
                'required',
                'date_format:H:i',
                new EventTimeRange,
            ],
            'name' => ['required'],
            'contact' => ['required'],
        ];
    }

    public function messages(): array
    {
        return ['date.unique' => 'Event has already been bidden.'];
    }

    /**
     * @param Validator $validator
     * @throws HttpResponseException
     */
    protected function failedValidation(Validator $validator)
    {
        $jsonResponse = response()->json(['Error' => $validator->errors()->all()], 422);

        throw new HttpResponseException($jsonResponse);
    }
}
