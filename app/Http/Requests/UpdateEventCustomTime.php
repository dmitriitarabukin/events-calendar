<?php

namespace App\Http\Requests;

use App\EventCustomTime;
use App\Rules\EventTimeRange;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class UpdateEventCustomTime extends FormRequest
{
    public function rules(): array
    {
        return [
            'time' => [
                'required',
                'date_format:H:i',
                Rule::unique(EventCustomTime::class)->where(function($query) {
                    return $query
                        ->where('event_custom_id', $this->event_custom_id)
                        ->where('time', $this->time);
                })->ignore($this->id),
                new EventTimeRange,
            ]
        ];
    }

    public function messages(): array
    {
        return ['time.unique' => 'EventCustomTime with the same event_custom_id and time already exists.'];
    }

    /**
     * @param Validator $validator
     * @throws HttpResponseException
     */
    protected function failedValidation(Validator $validator)
    {
        $jsonResponse = response()->json(['Error' => $validator->errors()->all()], 422);

        throw new HttpResponseException($jsonResponse);
    }
}
