<?php

namespace App\Http\Requests;

use App\Event;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class StoreEvent extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => [
                'required',
                Rule::unique(Event::class, 'name'),
            ],
        ];
    }

    public function messages(): array
    {
        return ['name.unique' => 'Event with the same name has already been stored.'];
    }

    /**
     * @param Validator $validator
     * @throws HttpResponseException
     */
    protected function failedValidation(Validator $validator)
    {
        $jsonResponse = response()->json(['Error' => $validator->errors()->all()], 422);

        throw new HttpResponseException($jsonResponse);
    }
}
