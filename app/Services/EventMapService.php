<?php
declare(strict_types=1);

namespace App\Services;


use App\Event;
use App\EventCustom;
use App\EventCustomTime;
use App\EventTime;
use App\Exceptions\EventMapServiceException;
use App\TimeMapItem;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class EventMapService
{
    private const DEFAULT_MAX_DATE_DAYS = 7;

    /** @var Event */
    private $event;

    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    /**
     * Returns time map collection for specific event and time period
     *
     * @param string $date
     * @param int $eventId
     * @param string|null $maxDate
     * @return Collection
     * @throws EventMapServiceException
     */
    public function findTimeMap(string $date, int $eventId, string $maxDate = null): Collection
    {
        $dateParsed = $this->parseAndValidateDate($date);
        $maxDateParsed = $this->parseAndValidateMaxDate($dateParsed, $maxDate);

        try {
            $event = $this->event->findEventWithRelationsById($eventId, ['eventTimes', 'eventCustoms.eventCustomTimes', 'eventBids']);
        } catch (ModelNotFoundException $exception) {
            throw new EventMapServiceException('Event with id ' . $eventId . ' was not found.');
        }

        $availableTimes = (new Collection([
            $this->findAvailableEventTimesByDateAndMaxDateForSpecificEvent($dateParsed, $maxDateParsed, $event),
            $this->findAvailableEventCustomsByDateAndMaxDateForSpecificEvent($dateParsed, $maxDateParsed, $event),
        ]))->flatten();

        return $this->mapBiddenEvents($event->eventBids()->get(), $availableTimes);
    }

    /**
     * Returns time map collection for specific date and for all events
     *
     * @param string $date
     * @return Collection
     * @throws EventMapServiceException
     */
    public function findMapByDate(string $date): Collection
    {
        $dateParsed = $this->parseAndValidateDate($date);

        $events = $this->event->findAllEventsWithRelations(['eventTimes', 'eventCustoms.eventCustomTimes', 'eventBids']);

        $availableTimes = (new Collection([
            $this->findAvailableEventTimesByDateForSpecificEvents($dateParsed, $events),
            $this->findAvailableEventCustomsByDateForSpecificEvents($dateParsed, $events),
        ]))->flatten();

        return $this->mapBiddenEvents($events->pluck('eventBids')->flatten(), $availableTimes);
    }

    /**
     * @param string $date
     * @return Carbon
     * @throws EventMapServiceException
     */
    private function parseAndValidateDate(string $date): Carbon
    {
        $date = Carbon::parse($date);

        if ($date->lessThan(Carbon::today())) {
            throw new EventMapServiceException('date should be more than or equal to now.');
        }

        return $date;
    }

    /**
     * @param Carbon $date
     * @param string|null $maxDate
     * @return Carbon
     * @throws EventMapServiceException
     */
    private function parseAndValidateMaxDate(Carbon $date, ?string $maxDate): Carbon
    {
        if (is_null($maxDate) === false) {
            $maxDate = Carbon::parse($maxDate);
            if ($maxDate->lessThanOrEqualTo($date)) {
                throw new EventMapServiceException('maxDate should be greater than date.');
            }
        } else {
            $maxDate = Carbon::parse($date);
            $maxDate->addDays(self::DEFAULT_MAX_DATE_DAYS);
        }

        return $maxDate;
    }

    /**
     * @param Carbon $date
     * @param Carbon $maxDate
     * @param Model $event
     * @return Collection
     */
    private function findAvailableEventTimesByDateAndMaxDateForSpecificEvent(Carbon $date, Carbon $maxDate, Model $event): Collection
    {
        $availableEventTimes = new Collection();

        $event
            ->eventTimes()
            ->each(function(EventTime $eventTime) use ($date, $maxDate, $event, $availableEventTimes) {
                $currentDate = new Carbon($date);

                if ($currentDate->weekday() !== (int)$eventTime->week_day) {
                    $currentDate->next((int)$eventTime->week_day);
                }

                while ($currentDate->lessThanOrEqualTo($maxDate)) {
                    $availableEventTimes->push(new TimeMapItem([
                        'date' => $currentDate->format('Y-m-d'),
                        'time' => $eventTime->time,
                        'eventName' => $event->name,
                    ]));
                    $currentDate->next((int)$eventTime->week_day);
                }
            });

        return $availableEventTimes;
    }

    /**
     * @param Carbon $date
     * @param Carbon $maxDate
     * @param Model $event
     * @return Collection
     */
    private function findAvailableEventCustomsByDateAndMaxDateForSpecificEvent(Carbon $date, Carbon $maxDate, Model $event): Collection
    {
        $availableEventCustoms = new Collection();

        $event
            ->eventCustoms()
            ->each(function(EventCustom $eventCustom) use ($date, $maxDate, $event, $availableEventCustoms) {
                $eventCustomDateParsed = Carbon::parse($eventCustom->date);
                if ($eventCustomDateParsed->betweenIncluded($date, $maxDate)) {
                    $eventCustom
                        ->eventCustomTimes()
                        ->each(function(EventCustomTime $eventCustomTime) use ($eventCustomDateParsed, $event, $availableEventCustoms) {
                            $availableEventCustoms->push(new TimeMapItem([
                                'date' => $eventCustomDateParsed->format('Y-m-d'),
                                'time' => $eventCustomTime->time,
                                'eventName' => $event->name,
                            ]));
                    });
                }
            });

        return $availableEventCustoms;
    }

    /**
     * @param Carbon $dateParsed
     * @param Collection $events
     * @return Collection
     */
    private function findAvailableEventTimesByDateForSpecificEvents(Carbon $dateParsed, Collection $events): Collection
    {
        $availableEventTimes = new Collection();

        $events->each(function(Event $event) use ($dateParsed, $availableEventTimes) {
                $event
                    ->eventTimes()
                    ->each(function(EventTime $eventTime) use ($dateParsed, $event, $availableEventTimes) {
                        if ($dateParsed->weekday() === (int)$eventTime->week_day) {
                            $availableEventTimes->push(new TimeMapItem([
                                'date' => $dateParsed->format('Y-m-d'),
                                'time' => $eventTime->time,
                                'eventName' => $event->name,
                            ]));
                        }
                    });
            });

        return $availableEventTimes;
    }

    /**
     * @param Carbon $date
     * @param Collection $events
     * @return Collection
     */
    private function findAvailableEventCustomsByDateForSpecificEvents(Carbon $date, Collection $events): Collection
    {
        $availableEventCustoms = new Collection();

        $events->each(function(Event $event) use ($date, $availableEventCustoms) {
            $event
                ->eventCustoms()
                ->each(function(EventCustom $eventCustom) use ($date, $event, $availableEventCustoms) {
                    $eventCustomDateParsed = Carbon::parse($eventCustom->date);
                    if ($eventCustomDateParsed->equalTo($date)) {
                        $eventCustom
                            ->eventCustomTimes()
                            ->each(function(EventCustomTime $eventCustomTime) use ($eventCustomDateParsed, $event, $availableEventCustoms) {
                                $availableEventCustoms->push(new TimeMapItem([
                                    'date' => $eventCustomDateParsed->format('Y-m-d'),
                                    'time' => $eventCustomTime->time,
                                    'eventName' => $event->name,
                                ]));
                            });
                    }
                });
        });

        return $availableEventCustoms;
    }

    /**
     * @param Collection $eventBids
     * @param Collection $availableTimes
     * @return Collection
     */
    private function mapBiddenEvents(Collection $eventBids, Collection $availableTimes): Collection
    {
        $timeMap = new Collection();

        $availableTimes->each(function(TimeMapItem $timeMapItem) use ($eventBids, $timeMap) {
            $biddenEvent = $eventBids
                ->where('date', $timeMapItem->date)
                ->where('time', $timeMapItem->time)
                ->first();

            if (is_null($biddenEvent) === true) {
                $timeMapItem->isFree = true;
            } else {
                $timeMapItem->isFree = false;
            }

            $timeMap->push($timeMapItem);
        });

        return $timeMap;
    }
}
