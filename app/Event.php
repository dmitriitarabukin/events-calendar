<?php

namespace App;

use App\Exceptions\EventBidException;
use App\Exceptions\EventCustomException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Event extends Model
{
    protected $fillable = [
        'name',
        'description',
    ];

    public function eventTimes()
    {
        return $this->hasMany(EventTime::class);
    }

    public function eventCustoms()
    {
        return $this->hasMany(EventCustom::class);
    }

    public function eventBids()
    {
        return $this->hasMany(EventBid::class);
    }

    /**
     * @param array $payload
     * @return Model
     */
    public function addEventTime(array $payload): Model
    {
        return $this
            ->eventTimes()
            ->create($payload);
    }

    /**
     * @param array $payload
     * @return Model
     */
    public function addEventCustom(array $payload): Model
    {
        return $this
            ->eventCustoms()
            ->create($payload);
    }

    /**
     * @param array $payload
     * @return Model
     */
    public function addEventBid(array $payload): Model
    {
        return $this
            ->eventBids()
            ->create($payload);
    }

    /**
     * @param array $payload
     * @return Model
     * @throws EventBidException
     */
    public function tryAddEventBid(array $payload): Model
    {
        if (($this->isEventTimeAvailableForBidding($payload) === false)
            && ($this->isEventCustomAvailableForBidding($payload) === false)) {
            throw new EventBidException('Chosen date and time are not available for bidding: ' . $payload['date'] . ' ' . $payload['time'] . '.');
        }

        return $this->addEventBid($payload);
    }

    /**
     * @param array $payload
     * @return Model
     * @throws EventCustomException
     */
    public function tryAddEventCustom(array $payload): Model
    {
        if (($this->isEventCustomFree($payload) === false)) {
            throw new EventCustomException('Chosen date is not available: ' . $payload['date'] . '.');
        }

        return $this->addEventCustom($payload);
    }

    /**
     * @param int $eventId
     * @param array $relations
     * @return Model
     */
    public function findEventWithRelationsById(int $eventId, array $relations): Model
    {
        return $this
            ->with($relations)
            ->where('id', $eventId)
            ->firstOrFail();
    }

    /**
     * @param array $relations
     * @return Collection
     */
    public function findAllEventsWithRelations(array $relations): Collection
    {
        return $this
            ->with($relations)
            ->get();
    }

    /**
     * @param array $eventBid
     * @return bool
     */
    public function isEventTimeAvailableForBidding(array $eventBid): bool
    {
        $eventTime = $this
            ->eventTimes()
            ->where('week_day', (string)Carbon::parse($eventBid['date'])->dayOfWeek)
            ->where('time', $eventBid['time'])
            ->first();

        return ($eventTime instanceof EventTime);
    }

    /**
     * @param array $eventBid
     * @return bool
     */
    public function isEventCustomAvailableForBidding(array $eventBid): bool
    {
        $eventCustom = $this
            ->eventCustoms()
            ->where('date', $eventBid['date'])
            ->whereHas('eventCustomTimes', function(Builder $query) use ($eventBid) {
                $query->where('time', $eventBid['time']);
            })
            ->first();

        return ($eventCustom instanceof EventCustom);
    }

    /**
     * @param array $eventCustom
     * @return bool
     */
    public function isEventCustomFree(array $eventCustom): bool
    {
        $eventCustomWeekday = Carbon::parse($eventCustom['date'])->weekday();

        $eventTimes = $this->eventTimes()->where('week_day', $eventCustomWeekday)->get();

        // Max number of event times for event = 14 (08:00-21:00)
        return ($eventTimes->count() < 14);
    }
}
