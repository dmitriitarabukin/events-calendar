<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeMapItem extends Model
{
    protected $fillable = [
        'date',
        'time',
        'eventName',
        'isFree',
    ];
}
