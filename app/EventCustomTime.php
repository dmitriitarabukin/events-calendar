<?php

namespace App;

use App\Exceptions\EventCustomException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class EventCustomTime extends Model
{
    protected $fillable = [
        'event_custom_id',
        'time',
    ];

    public function eventCustom()
    {
        return $this->belongsTo(EventCustom::class);
    }

    /**
     * @param array $payload
     * @throws EventCustomException
     */
    public function tryUpdateEventCustomTime(array $payload)
    {
        if ($this->eventCustom->isEventCustomTimeFree($payload) === false) {
            throw new EventCustomException('Event time already exists for this date and time: ' . Carbon::parse($this->eventCustom->date)->format('Y-m-d') . ' ' . $payload['time'] . '.');
        }

        $this->update($payload);
    }
}
