<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventTime extends Model
{
    protected $fillable = [
        'week_day',
        'time',
    ];

    public function event()
    {
        return $this->belongsTo(Event::class);
    }
}
