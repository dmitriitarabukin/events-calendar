<?php

namespace App;

use App\Exceptions\EventCustomException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class EventCustom extends Model
{
    protected $fillable = [
        'date',
        'event_id',
    ];

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    public function eventCustomTimes()
    {
        return $this->hasMany(EventCustomTime::class);
    }

    public function addEventCustomTime(array $payload): Model
    {
        return $this
            ->eventCustomTimes()
            ->create($payload);
    }

    /**
     * @param array $payload
     * @return Model
     * @throws EventCustomException
     */
    public function tryAddEventCustomTime(array $payload): Model
    {
        if ($this->isEventCustomTimeFree($payload) === false) {
            throw new EventCustomException('Event time already exists for this date and time: ' . Carbon::parse($this->date)->format('Y-m-d') . ' ' . $payload['time'] . '.');
        }

        return $this->addEventCustomTime($payload);
    }

    /**
     * @param array $payload
     * @throws EventCustomException
     */
    public function tryUpdateEventCustom(array $payload): void
    {
        if ($this->event->isEventCustomFree($payload) === false) {
            throw new EventCustomException('Event time already exists for this date: ' . $payload['date'] . '.');
        }

        $this->update($payload);
    }

    /**
     * @param array $eventCustomTime
     * @return bool
     */
    public function isEventCustomTimeFree(array $eventCustomTime): bool
    {
        $eventCustomWeekDay = (string)Carbon::parse($this->date)->weekday();

        $eventTime = EventTime::where('week_day', $eventCustomWeekDay)->where('time', $eventCustomTime['time'])->first();

        return (is_null($eventTime) === true ? true : false);
    }
}
