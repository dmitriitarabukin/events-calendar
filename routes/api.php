<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('/events','EventController');

Route::get('/event_times', 'EventTimeController@index')->name('event_times.index');
Route::apiResource('events.event_times', 'EventTimeController')->shallow();

Route::get('/event_custom', 'EventCustomController@index')->name('event_custom.index');
Route::apiResource('events.event_custom', 'EventCustomController')->shallow();

Route::get('/event_custom_times', 'EventCustomTimeController@index')->name('event_custom_times.index');
Route::apiResource('event_custom.event_custom_times', 'EventCustomTimeController')->shallow();

Route::get('/event_bids', 'EventBidController@index')->name('event_bids.index');
Route::apiResource('events.event_bids', 'EventBidController')->shallow();
